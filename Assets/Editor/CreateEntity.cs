﻿using Assets.Scripts.CardScripts.EntityScripts;
using Assets.Scripts.CardScripts.EntityScripts.SpecialAbility;
using Assets.Scripts.Dialogue;
using Assets.Scripts.Dialogue.Dialogues;
using Assets.Scripts.Game.LevelScripts;
using Assets.Scripts.Game.LevelScripts.LoseConditions;
using Assets.Scripts.Game.LevelScripts.WinConditions;
using System.IO;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;

public class CreateEntity : MonoBehaviour
{
    [MenuItem("Tools/Create new Entity")]
    static void WriteEntity()
    {
        var serializer = new XmlSerializer(typeof(Entity));
        using (var stream = new FileStream(@"Assets/Resources/" + new Entity().Path + @"/new.xml", FileMode.Create))
        {
            var newEntity = new Entity()
            {
                Name = "Bob, the Doorstop",
                Cost = 2,
                Subtext = "He is currently retired, because he has a doorstop shop.",
                Damage = 3,
                EntityOrigin = Assets.Scripts.CardScripts.Enums.EntityOrigin.Living_Construct,
                EntityType = Assets.Scripts.CardScripts.Enums.EntityType.Character,
                Health = 5,
                Priority = 70,
                Identifier = "Bob",
                Text = "First Strike, Reduces cost of All entities by 2, Reduces cost of Commoners and Characters by 1, Adds Solares and Daisy to the deck.",
                Abilities = new EntityAbility[]
                {
                    new AddCardToBattlefield() { Cards = new string[] { "Barricade", "Pidgeon"}},
                    new AddCardToDeck() { Cards = new string[] { "Solares", "Daisy"}},
                    new FirstStrike(),
                    new ReduceCostOfOrigin() { Origin = Assets.Scripts.CardScripts.Enums.EntityOrigin.All, ReductionAmount = 2},
                    new ReduceCostOfType() { ReductionAmount = 1, Type = Assets.Scripts.CardScripts.Enums.EntityType.Commoner},
                    new ReduceCostOfType() { ReductionAmount = 1, Type = Assets.Scripts.CardScripts.Enums.EntityType.Character},
                    new SpawnUnit() { Cards = new string[] { "Sena", "Pidgeon"}}
                }
            };
            serializer.Serialize(stream, newEntity);
        }
    }

    [MenuItem("Tools/Create new Level")]
    static void WriteLevel()
    {
        var serializer = new XmlSerializer(typeof(Level));
        using (var stream = new FileStream(@"Assets/Resources/Levels/new.xml", FileMode.Create))
        {
            var newLevel = new Level()
            {
                Enemies = new EnemiesAmount[] {
                    new EnemiesAmount() {  Amount = 2, Card = "Pidgeon" },
                    new EnemiesAmount() {  Amount = 1, Card = "Artemis" },
                    new EnemiesAmount() {  Amount = 1, Card = "Sena" }},
                LoseConditions = new LoseCondition[] { new LoseCommander(), new LoseUnit() { UnitIdentifier = "Artemis" } },
                WinConditions = new WinCondition[] { new KillAllEnemies(), new KillEnemy() { EnemyIdentifier = "Tyler" } },
                UnlockedCards = new string[] { "Leontien", "Ayashe", "Ash" }
            };
            serializer.Serialize(stream, newLevel);
        }
    }

    [MenuItem("Tools/Create new Dialogue Tree")]
    static void WriteDialogue()
    {
        var serializer = new XmlSerializer(typeof(DialogueTree));
        using (var stream = new FileStream(@"Assets/Resources/Dialogues/new.xml", FileMode.Create))
        {
            var newDialogueTree = new DialogueTree()
            {
                Environment = "Sena's home",
                Dialogues = new DialogueLine[]
                {
                    new SingleLine() { Id = "1-1", Speaker = "Sena", Text = "What happened?", FollowupId="1-2"},
                    new SingleLine() { Id = "1-2", Speaker = "Sena", Text = "And who are you?", FollowupId="1-3"},
                    new SingleLine() { Id = "1-3", Speaker = "Artemis", Text = "Meow, I am artemis.", FollowupId="1-4"},
                    new Question() { Id = "1-4", Speaker = "Sena", Text = "What should I do with you?", Answers = new Answer[]
                    {
                        new Answer() { Id = "1-4-1", Speaker = "Sena", Text = "Kill you.", FollowupId = "1-5-1" },
                        new Answer() { Id = "1-4-2", Speaker = "Sena", Text = "Befriend you.", FollowupId = "1-5-2" },
                    }},
                    new SingleLine() { Id = "1-5-1", Speaker = "Artemis", Text = "What??!", FollowupId=""},
                    new SingleLine() { Id = "1-5-2", Speaker = "Artemis", Text = "Yay!", FollowupId=""},
                }
            };
            serializer.Serialize(stream, newDialogueTree);
        }
    }

    [MenuItem("Tools/Read Entity")]
    static void ReadEntity()
    {
        var serializer = new XmlSerializer(typeof(Entity));
        using (var stream = new FileStream(@"Assets/Resources/" + new Entity().Path + @"/new.xml", FileMode.Open))
        {
            var newEntity = serializer.Deserialize(stream) as Entity;
            Debug.Log(newEntity.Name + "  " + newEntity.Text);
            Debug.Log(newEntity.Abilities.Length);
            Debug.Log(((ReduceCostOfOrigin)(newEntity.Abilities[2])).Origin);
        }
    }
}
