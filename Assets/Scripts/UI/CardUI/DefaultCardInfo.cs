﻿using System;
using Assets.Scripts.CardScripts;
using Assets.Scripts.CardScripts.EntityScripts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.CardUI
{
    public class DefaultCardInfo : MonoBehaviour
    {
        [SerializeField] private EntityCardInfo _entityCardInfo;
        [SerializeField] private Image _image;
        [SerializeField] private Text _cost;
        [SerializeField] private Text _name;
        [SerializeField] private Text _origin;

        public void Change(Card card)
        {
            if (card is Entity entity)
            {
                _entityCardInfo.Change(entity);
                _origin.text = card.GetType().Name + " - " + entity.EntityOrigin;
            }
            else
            {
                _origin.text = card.GetType().Name;
            }

            _image.sprite = Resources.Load<Sprite>("Images/Cards/" + card.Identifier);
            _cost.text = card.Cost.ToString();
            _name.text = card.Name.ToString();
        }

        public void Face(bool left)
        {
            if (left)
                _image.transform.localEulerAngles = new Vector3(0, 180, 0);
            else
                _image.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }
}
