﻿using Assets.Scripts.CardScripts;
using Assets.Scripts.ObjectPooling;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.CardUI
{
    public class VisibleCard : MonoBehaviour, IPoolable<VisibleCard>
    {
        private const int DefaultWidth = 600, DefaultHeight = 900;

        [SerializeField] private DefaultCardInfo _defaultCardInfo;
        [SerializeField] private ExtendedCardInfo _extendedCardInfo;
        [SerializeField] private Button _buttonOfCard;

        public Card ContainingCard { get; private set; }

        public void SetCard(Card card)
        {
            ContainingCard = card;
            _defaultCardInfo.Change(card);
            _extendedCardInfo.Change(card);
        }

        public void ResetCard()
        {
            Debug.Log(ContainingCard.Name + " is damaged.");
            // TODO check if values are different than origianl
            SetCard(ContainingCard);
        }

        public void ShowMinimalInfo()
        {
            _extendedCardInfo.gameObject.SetActive(false);
        }

        public void ShowMaximumInfo()
        {
            _extendedCardInfo.gameObject.SetActive(true);
        }

        public void AddListener(UnityAction action)
        {
            _buttonOfCard.onClick.AddListener(action);
        }

        public void SetInActive()
        {
            gameObject.SetActive(false);
            _buttonOfCard.onClick.RemoveAllListeners();
            Face(false);
        }

        public void SetActive()
        {
            gameObject.SetActive(true);
            transform.localScale = Vector3.one;
            Face(false);
        }

        public void Resize(Vector2 maxSize)
        {
            var scale = ScaleValue(maxSize);
            transform.localScale = new Vector3(scale, scale, scale);
        }

        private float ScaleValue(Vector2 maxSize)
        {
            var scaleWidth = maxSize.x / DefaultWidth;
            var scaleHeight = maxSize.y / DefaultHeight;
            return scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
        }

        public void Face(bool left)
        {
            _defaultCardInfo.Face(left);
        }
    }
}
