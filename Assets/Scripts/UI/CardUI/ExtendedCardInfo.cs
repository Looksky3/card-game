﻿using Assets.Scripts.CardScripts;
using Assets.Scripts.CardScripts.EntityScripts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.CardUI
{
    public class ExtendedCardInfo : MonoBehaviour
    {
        [SerializeField] private Text _priority;
        [SerializeField] private Text _text;
        [SerializeField] private Text _subText;

        public void Change(Card card)
        {
            if (card is Entity entity)
                _priority.text = entity.Priority.ToString();
            else
                _priority.text = "";

            _text.text = card.Text;
            _subText.text = card.Subtext;
        }
    }
}
