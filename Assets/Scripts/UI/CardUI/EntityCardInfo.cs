﻿using Assets.Scripts.CardScripts.EntityScripts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.CardUI
{
    public class EntityCardInfo : MonoBehaviour
    {
        [SerializeField] private Sprite _commonerIcon, _characterIcon, heroIcon;
        [SerializeField] private Image _typeIcon;
        [SerializeField] private Text _damage;
        [SerializeField] private Text _health;

        public void Change(Entity entity)
        {
            switch(entity.EntityType)
            {
                case CardScripts.Enums.EntityType.Character:
                    _typeIcon.sprite = _characterIcon;
                    break;
                case CardScripts.Enums.EntityType.Commoner:
                    _typeIcon.sprite = _commonerIcon;
                    break;
                case CardScripts.Enums.EntityType.Hero:
                    _typeIcon.sprite = heroIcon;
                    break;
            }

            _damage.text = entity.Damage.ToString();
            _health.text = entity.Health.ToString();
        }
    }
}
