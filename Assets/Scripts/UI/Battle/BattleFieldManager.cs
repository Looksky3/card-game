﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.CardScripts;
using Assets.Scripts.CardScripts.EntityScripts;
using Assets.Scripts.Game.Battle.CardHandler;
using Assets.Scripts.Game.Battle.Event;
using Assets.Scripts.ObjectPooling;
using Assets.Scripts.UI.CardUI;
using Assets.Scripts.UI.Factories;
using UnityEngine;

namespace Assets.Scripts.UI.Battle
{
    public class BattleFieldManager : MonoBehaviour
    {
        private const int border = 10;

        [SerializeField] private RectTransform _handArea;
        [SerializeField] private RectTransform _playAreaPlayer;
        [SerializeField] private RectTransform _playAreaEnemy;
        [SerializeField] private VisibleCard _cardPrefab;

        [SerializeField] private CardFactory _cardFactory;
        private PoolManager<VisibleCard> _poolManager;
        private List<VisibleCard> _playerCards = new List<VisibleCard>();
        private List<VisibleCard> _enemyCards = new List<VisibleCard>();
        private List<VisibleCard> _handCards = new List<VisibleCard>();

        private void Awake()
        {
            _poolManager = new PoolManager<VisibleCard>(_cardFactory);
            Initialize();
        }

        private void OnGameEnds(bool playerWins)
        {
            Uninitialize();
        }

        private void Initialize()
        {
            CardsDrawnEvent.OnCardsDrawn += OnCardsDrawn;
            CardIsPlayedEvent.OnCardIsPlayed += OnCardIsPlayed;
            NewRoundBeginsEvent.OnRoundBegins += OnRoundBegins;
            PlayerCardEntersBattlefieldEvent.OnPlayerCardEntersBattlefield += OnPlayerCardEntersBattlefield;
            EnemySpawnedEvent.OnEnemySpawned += OnEnemySpawned;
            CardIsAttackedEvent.OnCardIsAttacked += OnCardIsAttacked;
            CardIsDestroyedEvent.OnCardIsDestroyed += OnCardIsDestroyed;
            GameEndsEvent.OnGameEnds += OnGameEnds;
        }

        private void Uninitialize()
        {
            CardsDrawnEvent.OnCardsDrawn -= OnCardsDrawn;
            CardIsPlayedEvent.OnCardIsPlayed -= OnCardIsPlayed;
            NewRoundBeginsEvent.OnRoundBegins -= OnRoundBegins;
            PlayerCardEntersBattlefieldEvent.OnPlayerCardEntersBattlefield -= OnPlayerCardEntersBattlefield;
            EnemySpawnedEvent.OnEnemySpawned -= OnEnemySpawned;
            CardIsAttackedEvent.OnCardIsAttacked -= OnCardIsAttacked;
            CardIsDestroyedEvent.OnCardIsDestroyed -= OnCardIsDestroyed;
            GameEndsEvent.OnGameEnds -= OnGameEnds;

            _handCards.Clear();
            SetHand();

            _poolManager.RemoveAllObjects();

            _playerCards.Clear();
            SetPlayArea(_playerCards, _playAreaPlayer);

            _enemyCards.Clear();
            SetPlayArea(_enemyCards, _playAreaEnemy);
        }

        private void OnRoundBegins()
        {
            ClearHand();
        }

        private void OnCardIsAttacked(Entity attackedCard, Entity attacker, bool isPlayerCard)
        {
            if (isPlayerCard)
                _playerCards.First(c => c.ContainingCard == attackedCard).ResetCard();
            else
                _enemyCards.First(c => c.ContainingCard == attackedCard).ResetCard();
        }

        private void OnCardIsDestroyed(Entity destroyedCard, bool isPlayerCard)
        {
            if (isPlayerCard)
            {
                var visiblecard = _playerCards.First(c => c.ContainingCard == destroyedCard);
                _playerCards.Remove(visiblecard);
                _poolManager.RemoveObject(visiblecard);
                SetPlayArea(_playerCards, _playAreaPlayer);
            }
            else
            {
                var visiblecard = _enemyCards.First(c => c.ContainingCard == destroyedCard);
                _enemyCards.Remove(visiblecard);
                _poolManager.RemoveObject(visiblecard);
                SetPlayArea(_enemyCards, _playAreaEnemy);
            }
        }

        private void ClearHand()
        {
            var cards = _handCards.Select(c => c.ContainingCard);
            CardDiscardedEvent.Call(cards.ToArray());
            foreach (var card in _handCards)
            {
                _poolManager.RemoveObject(card);
            }
            _handCards.Clear();
            SetHand();
        }

        private void OnCardsDrawn(Card[] drawnCards)
        {
            foreach (var card in drawnCards)
            {
                var newCard = _poolManager.GetObject();
                newCard.AddListener(() => { Play(newCard); });
                newCard.SetCard(card);
                newCard.ShowMaximumInfo();
                _handCards.Add(newCard);
            }
            SetHand();
        }

        private void Play(VisibleCard card)
        {
            CardPlayer.TryPlay(card.ContainingCard);
        }

        private void SetHand()
        {
            var sizeColumn = _handArea.sizeDelta.x / _handCards.Count;
            int x = 0;
            foreach (var card in _handCards)
            {
                card.Resize(new Vector2(sizeColumn - border, _handArea.sizeDelta.y));
                card.transform.position = _handArea.TransformPoint(new Vector3(_handArea.rect.min.x + ((x + 0.5f) * sizeColumn), _handArea.rect.min.y + 0.5f * _handArea.sizeDelta.y, card.transform.position.z));
                x++;
            }
        }

        private void OnCardIsPlayed(Card playedCard)
        {
            foreach (VisibleCard card in _handCards)
            {
                if (card.ContainingCard == playedCard)
                {
                    _poolManager.RemoveObject(card);
                    _handCards.Remove(card);
                    SetHand();
                    break;
                }
            }
        }

        private void OnPlayerCardEntersBattlefield(Card card)
        {
            if (card is Entity entity)
            {
                var newCard = _poolManager.GetObject();
                newCard.SetCard(entity);
                _playerCards.Add(newCard);
                SetPlayArea(_playerCards, _playAreaPlayer);
            }
        }

        private void OnEnemySpawned(Entity enemy)
        {
            var newCard = _poolManager.GetObject();
            newCard.SetCard(enemy);
            _enemyCards.Add(newCard);
            SetPlayArea(_enemyCards, _playAreaEnemy);
            newCard.Face(true);
        }

        private void SetPlayArea(List<VisibleCard> cards, RectTransform rectTransform)
        {
            var leftDownPosition = rectTransform.rect.min;
            var cardVisibleCardPairing = cards.Select(c => new Tuple<Entity, VisibleCard>(c.ContainingCard as Entity, c));
            IEnumerable<IGrouping<int, Tuple<Entity, VisibleCard>>> cardsByPriority = null;
            if (rectTransform == _playAreaEnemy)
                cardsByPriority = cardVisibleCardPairing.OrderByDescending(c => c.Item1.Priority).GroupBy(c => c.Item1.Priority);
            else
                cardsByPriority = cardVisibleCardPairing.OrderBy(c => c.Item1.Priority).GroupBy(c => c.Item1.Priority);
            var amountOfColumns = cardsByPriority.Count();
            var sizeColumn = rectTransform.sizeDelta.x / amountOfColumns;
            int x = 0;
            foreach (var cardsOfPriority in cardsByPriority)
            {
                int y = 0;
                var amountOfCardsOfPriority = cardsOfPriority.Count();
                var height = rectTransform.sizeDelta.y / amountOfCardsOfPriority;
                foreach (var card in cardsOfPriority)
                {
                    card.Item2.ShowMinimalInfo();
                    card.Item2.Resize(new Vector2(sizeColumn - border, height - border));
                    card.Item2.transform.position = rectTransform.TransformPoint(new Vector3(leftDownPosition.x + ((x + 0.5f) * sizeColumn), leftDownPosition.y + ((y + 0.5f) * height), card.Item2.transform.position.z));
                    y++;
                }
                x++;
            }
        }

        public void EndRound()
        {
            RoundEndsEvent.Call();
        }
    }
}
