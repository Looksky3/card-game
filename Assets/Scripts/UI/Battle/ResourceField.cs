﻿using System;
using Assets.Scripts.Game.Battle.Event;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Battle
{
    [RequireComponent(typeof(Text))]
    public class ResourceField : MonoBehaviour
    {
        private void Awake()
        {
            ResourcesChangedEvent.OnResourcedChanged += OnResourcesChanged;
        }

        private void OnResourcesChanged(int newAmount)
        {
            GetComponent<Text>().text = newAmount.ToString();
        }
    }
}
