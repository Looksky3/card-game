﻿using Assets.Scripts.ObjectPooling;
using Assets.Scripts.UI.Dialogue;
using UnityEngine;

namespace Assets.Scripts.UI.Factories
{
    public class AnswerFactory : MonoBehaviour, IPoolableFactory<AnswerButton>
    {
        [SerializeField] private AnswerButton _prefab;

        public AnswerButton Create()
        {
            var newBug = GameObject.Instantiate(_prefab.gameObject);
            newBug.transform.SetParent(transform, false);
            return newBug.GetComponent<AnswerButton>();
        }
    }
}
