﻿using Assets.Scripts.ObjectPooling;
using Assets.Scripts.UI.CardUI;
using UnityEngine;

namespace Assets.Scripts.UI.Factories
{
    public class CardFactory : MonoBehaviour, IPoolableFactory<VisibleCard>
    {
        [SerializeField] private VisibleCard _prefab;
        
        public VisibleCard Create()
        {
            var newBug = GameObject.Instantiate(_prefab.gameObject);
            newBug.transform.SetParent(transform, false);
            return newBug.GetComponent<VisibleCard>();
        }
    }
}
