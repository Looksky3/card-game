﻿using Assets.Scripts.Dialogue.Dialogues;
using Assets.Scripts.Dialogue.Events;
using Assets.Scripts.ObjectPooling;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Dialogue
{
    public class AnswerButton : MonoBehaviour, IPoolable<AnswerButton>
    {
        [SerializeField] private Text _myText;
        [SerializeField] private Button _button;

        public void SetInActive()
        {
            gameObject.SetActive(false);
            _button.onClick.RemoveAllListeners();
        }

        public void SetActive()
        {
            gameObject.SetActive(true);
        }

        public void Initialize(Answer answer)
        {
            _myText.text = answer.Text;
            _button.onClick.AddListener(() => QuestionEndsEvent.OnQuestionEnds(answer));
        }
    }
}
