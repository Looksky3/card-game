﻿using Assets.Scripts.Dialogue.Dialogues;
using Assets.Scripts.Dialogue.Events;
using Assets.Scripts.ObjectPooling;
using Assets.Scripts.UI.Factories;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Dialogue
{
    public class DialogueFieldManager : MonoBehaviour
    {
        [SerializeField] private Image _speakerPortrait;
        [SerializeField] private Image _environment;
        [SerializeField] private Text _dialogue;
        [SerializeField] private Button _continueButton;

        [SerializeField] private RectTransform _answerField;
        [SerializeField] private AnswerFactory _answerFactory;
        private PoolManager<AnswerButton> _poolManager;

        private void Awake()
        {
            DialogueStartsEvent.OnDialogueStarts += OnDialogueStarts;
            SingleLineStartsEvent.OnSingleLineStarts += OnSingleLineStarts;
            QuestionStartsEvent.OnQuestionStarts += OnQuestionStarts;
            QuestionEndsEvent.OnQuestionEnds += OnQuestionEnds;
            _poolManager = new PoolManager<AnswerButton>(_answerFactory);
        }

        private void OnDialogueStarts(string environment)
        {
            _environment.sprite = Resources.Load<Sprite>("Images/Environments/" + environment);
        }

        private void OnSingleLineStarts(SingleLine line)
        {
            SetContinueButton(true);
            SetDialogueLine(line);
        }

        private void OnQuestionStarts(Question question)
        {
            SetContinueButton(false);
            SetDialogueLine(question);
            SetAnswers(question.Answers);
        }

        private void SetContinueButton(bool enabled)
        {
            if (_continueButton.enabled == enabled)
                return;

            _continueButton.enabled = enabled;
        }

        private void SetDialogueLine(DialogueLine line)
        {
            _dialogue.text = line.Text;
            _speakerPortrait.sprite = Resources.Load<Sprite>("Images/Cards/" + line.Speaker);
        }

        private void SetAnswers(Answer[] answers)
        {
            var sizeColumn = _answerField.sizeDelta.x / answers.Length;
            var x = 0;
            foreach (var answer in answers)
            {
                var answerButton = _poolManager.GetObject();
                answerButton.Initialize(answer);
                answerButton.transform.position = _answerField.TransformPoint(new Vector3(_answerField.rect.min.x + ((x + 0.5f) * sizeColumn), _answerField.rect.min.y + 0.5f * _answerField.sizeDelta.y, answerButton.transform.position.z));
                x++;
            }
        }

        private void OnQuestionEnds(Answer answer)
        {
            _poolManager.RemoveAllObjects();
        }

        public void LineRead()
        {
            SingleLineEndsEvent.Call();
        }
    }
}
