﻿namespace Assets.Scripts.CardScripts.Enums
{
    public enum EntityOrigin
    {
        Animal,
        Human,
        Construction,
        Living_Construct,
        Angel,
        Demon,
        Undead,
        All
    }
}
