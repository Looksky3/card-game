﻿namespace Assets.Scripts.CardScripts.Enums
{
    public enum CardType
    {
        None,
        Entity,
        Action,
        Enhancement
    }
}
