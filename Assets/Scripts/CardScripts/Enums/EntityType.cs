﻿namespace Assets.Scripts.CardScripts.Enums
{
    public enum EntityType
    {
        Commoner,
        Character,
        Hero
    }
}
