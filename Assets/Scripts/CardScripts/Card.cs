﻿using System.IO;
using System.Xml.Serialization;

namespace Assets.Scripts.CardScripts
{
    public abstract class Card
    {
        [XmlElement("Name")] public string Name = "Unnamed";
        [XmlElement("Cost")] public int Cost = 0;
        [XmlElement("Text")] public string Text = "Some text";
        [XmlElement("Subtext")] public string Subtext = "Some text";
        [XmlElement("Identifier")] public string Identifier = "Image";

        public abstract string Path { get; }

        public static T Read<T>(string name) where T : Card, new()
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new FileStream(@"Assets/Resources/" + new T().Path + @"/" + name + ".xml", FileMode.Open))
            {
                var readCard = serializer.Deserialize(stream) as T;
                return readCard;
            }
        }
    }
}
