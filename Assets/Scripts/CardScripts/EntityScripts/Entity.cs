﻿using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.CardScripts.EntityScripts.SpecialAbility;
using Assets.Scripts.CardScripts.Enums;

namespace Assets.Scripts.CardScripts.EntityScripts
{
    public class Entity : Card
    {
        [XmlElement("Priority")] public int Priority = 0;
        [XmlElement("Damage")] public int Damage = 0;
        [XmlElement("Health")] public int Health = 0;
        [XmlElement("Origin")] public EntityOrigin EntityOrigin = EntityOrigin.Human;
        [XmlElement("Type")] public EntityType EntityType = EntityType.Commoner;

        [XmlArrayItem(Type = typeof(AddCardToBattlefield))]
        [XmlArrayItem(Type = typeof(AddCardToDeck))]
        [XmlArrayItem(Type = typeof(FirstStrike))]
        [XmlArrayItem(Type = typeof(ReduceCostOfOrigin))]
        [XmlArrayItem(Type = typeof(ReduceCostOfType))]
        [XmlArrayItem(Type = typeof(SpawnUnit))]
        [XmlArray("Abilities"), XmlArrayItem("Ability")] public EntityAbility[] Abilities;

        public override string Path => @"Entity";

        public Entity Clone()
        {
            return new Entity()
            {
                Identifier = Identifier,
                Cost = Cost,
                Name = Name,
                Subtext = Subtext,
                Text = Text,
                Priority = Priority,
                Damage = Damage,
                Health = Health,
                EntityOrigin = EntityOrigin,
                EntityType = EntityType,
                Abilities = Abilities.Select(a => a.Clone()).ToArray()
            };
        }
    }
}
