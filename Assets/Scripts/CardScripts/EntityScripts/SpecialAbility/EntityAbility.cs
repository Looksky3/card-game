﻿namespace Assets.Scripts.CardScripts.EntityScripts.SpecialAbility
{
    public abstract class EntityAbility
    {
        public abstract EntityAbility Clone();
    }
}
