﻿using Assets.Scripts.CardScripts.Enums;
using System.Xml.Serialization;

namespace Assets.Scripts.CardScripts.EntityScripts.SpecialAbility
{
    public class ReduceCostOfType : EntityAbility
    {
        [XmlElement("ReductionAmount")] public int ReductionAmount;
        [XmlElement("Type")] public EntityType Type;

        public override EntityAbility Clone()
        {
            return new ReduceCostOfType()
            {
                ReductionAmount = ReductionAmount,
                Type = Type
            };
        }
    }
}
