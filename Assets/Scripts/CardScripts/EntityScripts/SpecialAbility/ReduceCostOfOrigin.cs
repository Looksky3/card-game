﻿using Assets.Scripts.CardScripts.Enums;
using System.Xml.Serialization;

namespace Assets.Scripts.CardScripts.EntityScripts.SpecialAbility
{
    public class ReduceCostOfOrigin : EntityAbility
    {
        [XmlElement("ReductionAmount")] public int ReductionAmount;
        [XmlElement("Origin")] public EntityOrigin Origin;

        public override EntityAbility Clone()
        {
            return new ReduceCostOfOrigin()
            {
                ReductionAmount = ReductionAmount,
                Origin = Origin
            };
        }
    }
}
