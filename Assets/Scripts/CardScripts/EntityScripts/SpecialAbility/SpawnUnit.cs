﻿using System.Xml.Serialization;

namespace Assets.Scripts.CardScripts.EntityScripts.SpecialAbility
{
    public class SpawnUnit : EntityAbility
    {
        [XmlArray("CardNames"), XmlArrayItem("Card")] public string[] Cards;

        public override EntityAbility Clone()
        {
            return new SpawnUnit()
            {
                Cards = Cards
            };
        }
    }
}
