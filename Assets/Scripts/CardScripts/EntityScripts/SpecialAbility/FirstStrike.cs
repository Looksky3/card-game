﻿namespace Assets.Scripts.CardScripts.EntityScripts.SpecialAbility
{
    public class FirstStrike : EntityAbility
    {
        public override EntityAbility Clone()
        {
            return new FirstStrike();
        }
    }
}
