﻿using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.LevelScripts.WinConditions
{
    public class KillAllEnemies : WinCondition
    {
        public bool IsCompleted(Entity[] enemies)
        {
            if (enemies.Length == 0)
            {
                Complete();
                return true;
            }
            return false;
        }
    }
}
