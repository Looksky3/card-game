﻿using Assets.Scripts.Game.Battle.Event;
using System.Xml.Serialization;

namespace Assets.Scripts.Game.LevelScripts.WinConditions
{
    public abstract class WinCondition
    {
        [XmlIgnoreAttribute] public bool Completed { get; private set; }

        protected void Complete()
        {
            Completed = true;
            WinConditionCompletedEvent.Call(this);
        }
    }
}
