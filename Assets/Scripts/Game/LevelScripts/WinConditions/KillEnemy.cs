﻿using System;
using System.Xml.Serialization;
using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.LevelScripts.WinConditions
{
    public class KillEnemy : WinCondition
    {
        [XmlElement("Enemy")] public string EnemyIdentifier;

        public bool IsCompleted(Entity enemy)
        {
            if (enemy.Identifier == EnemyIdentifier)
            {
                Complete();
                return true;
            }
            return false;
        }
    }
}
