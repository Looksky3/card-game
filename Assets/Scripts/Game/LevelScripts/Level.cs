﻿using Assets.Scripts.Game.LevelScripts.LoseConditions;
using Assets.Scripts.Game.LevelScripts.WinConditions;
using System.IO;
using System.Xml.Serialization;

namespace Assets.Scripts.Game.LevelScripts
{
    public class Level
    {
        [XmlArray("StartingEnemies"), XmlArrayItem("Enemy")] public EnemiesAmount[] Enemies;

        [XmlArrayItem(Type = typeof(KillAllEnemies))]
        [XmlArrayItem(Type = typeof(KillEnemy))]
        [XmlArray("WinConditions"), XmlArrayItem("WinCondition")] public WinCondition[] WinConditions;

        [XmlArrayItem(Type = typeof(LoseCommander))]
        [XmlArrayItem(Type = typeof(LoseUnit))]
        [XmlArray("LoseConditions"), XmlArrayItem("LoseCondition")] public LoseCondition[] LoseConditions;

        [XmlArray("UnlockedCards"), XmlArrayItem("Card")] public string[] UnlockedCards;

        public static Level Read(string name)
        {
            var serializer = new XmlSerializer(typeof(Level));
            using (var stream = new FileStream(@"Assets/Resources/Levels/" + name + ".xml", FileMode.Open))
            {
                var readCard = serializer.Deserialize(stream) as Level;
                return readCard;
            }
        }
    }
}
