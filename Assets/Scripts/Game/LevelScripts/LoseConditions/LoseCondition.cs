﻿using Assets.Scripts.Game.Battle.Event;
using System.Xml.Serialization;

namespace Assets.Scripts.Game.LevelScripts.LoseConditions
{
    public abstract class LoseCondition
    {
        [XmlIgnoreAttribute] public bool Completed { get; private set; }

        protected void Complete()
        {
            Completed = true;
            LoseConditionCompletedEvent.Call(this);
        }
    }
}
