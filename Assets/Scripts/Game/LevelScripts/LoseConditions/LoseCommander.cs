﻿using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.LevelScripts.LoseConditions
{
    public class LoseCommander : LoseCondition
    {
        public bool IsCompleted(Entity commander, Entity lostCard)
        {
            if (commander == lostCard)
            {
                Complete();
                return true;
            }
            return false;
        }
    }
}
