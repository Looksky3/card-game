﻿using Assets.Scripts.CardScripts.EntityScripts;
using System.Xml.Serialization;

namespace Assets.Scripts.Game.LevelScripts.LoseConditions
{
    public class LoseUnit : LoseCondition
    {
        [XmlElement("Unit")] public string UnitIdentifier;
        
        public bool IsCompleted(Entity unit)
        {
            if(unit.Identifier == UnitIdentifier)
            {
                Complete();
                return true;
            }
            return false;
        }
    }
}
