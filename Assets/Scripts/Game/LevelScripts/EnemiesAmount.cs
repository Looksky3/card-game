﻿using System.Xml.Serialization;

namespace Assets.Scripts.Game.LevelScripts
{
    public class EnemiesAmount
    {
        [XmlElement("Amount")] public int Amount;
        [XmlElement("Card")] public string Card;
    }
}
