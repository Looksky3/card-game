﻿using Assets.Scripts.CardScripts;
using Assets.Scripts.CardScripts.EntityScripts;
using Assets.Scripts.CardScripts.EntityScripts.SpecialAbility;
using Assets.Scripts.Game.Battle.Event;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Game
{
    public class Deck
    {
        private List<Card> _cardsInDeck, _cardsInDiscard;
        public Card[] CardsInDeck => _cardsInDeck.ToArray();

        public Deck(params Card[] cards)
        {
            _cardsInDeck = new List<Card>(cards);
            _cardsInDiscard = new List<Card>();
            CardIsPlayedEvent.OnCardIsPlayed += OnCardIsPlayed;
            PlayerCardEntersBattlefieldEvent.OnPlayerCardEntersBattlefield += OnPlayerCardEntersBattlefield;
            CardDiscardedEvent.OnCardsDiscarded += OnCardsDiscarded;
        }

        private void OnCardsDiscarded(Card[] cards)
        {
            _cardsInDiscard.AddRange(cards);
        }

        private void OnPlayerCardEntersBattlefield(Entity entity)
        {
            foreach (var specialAbility in entity.Abilities)
            {
                if (!(specialAbility is AddCardToDeck addCardToDeckAbility))
                    continue;

                foreach (var card in addCardToDeckAbility.Cards)
                    _cardsInDeck.Add(Card.Read<Entity>(card));
            }
        }

        private void OnCardIsPlayed(Card playedCard)
        {
            if (!(playedCard is Entity entity))
            {
                _cardsInDiscard.Add(playedCard);
                return;
            }

            if(entity.EntityType == CardScripts.Enums.EntityType.Commoner)
            {
                _cardsInDiscard.Add(entity.Clone());
            }
        }

        public void Draw(int drawAmount)
        {
            if (drawAmount == 0)
                return;

            if (drawAmount > _cardsInDeck.Count)
            {
                ShuffleDeck(_cardsInDiscard);
                _cardsInDeck.AddRange(_cardsInDiscard);
                _cardsInDiscard.Clear();
            }

            var cards = _cardsInDeck.Take(drawAmount).ToArray();
            if (drawAmount > _cardsInDeck.Count)
                _cardsInDeck.Clear();
            else
                _cardsInDeck.RemoveRange(0, drawAmount);

            CardsDrawnEvent.Call(cards);
        }

        private void ShuffleDeck(List<Card> deck)
        {
            var count = deck.Count;
            var last = count - 1;
            for (var i = 0; i < last; ++i)
            {
                var r = UnityEngine.Random.Range(i, count);
                var tmp = deck[i];
                deck[i] = deck[r];
                deck[r] = tmp;
            }
        }
    }
}
