﻿using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class PlayerCardEntersBattlefieldEvent
    {
        public delegate void PlayerCardEntersBattlefieldEventHandler(Entity card);
        public static PlayerCardEntersBattlefieldEventHandler OnPlayerCardEntersBattlefield;

        public static void Call(Entity card)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnPlayerCardEntersBattlefield?.Invoke(card));
        }
    }
}
