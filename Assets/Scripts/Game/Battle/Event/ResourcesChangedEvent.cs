﻿namespace Assets.Scripts.Game.Battle.Event
{
    public class ResourcesChangedEvent
    {
        private static int _resourceAmount;
        public static int ResourceAmount { get => _resourceAmount; set { Call(value); } }

        public delegate void ResourcesChangedEventHandler(int newAmount);
        public static ResourcesChangedEventHandler OnResourcedChanged;

        public static void Call(int newAmount)
        {
            _resourceAmount = newAmount;
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnResourcedChanged?.Invoke(newAmount));
        }
    }
}
