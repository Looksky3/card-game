﻿using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class CardIsAttackedEvent
    {
        public delegate void CardIsAttackedEventHandler(Entity attackedCard, Entity attacker, bool playerIsAttacked);
        public static CardIsAttackedEventHandler OnCardIsAttacked;

        public static void Call(Entity attackedCard, Entity attacker, bool playerIsAttacked)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnCardIsAttacked?.Invoke(attackedCard, attacker, playerIsAttacked));
        }
    }
}
