﻿using Assets.Scripts.Game.LevelScripts.LoseConditions;

namespace Assets.Scripts.Game.Battle.Event
{
    public class LoseConditionCompletedEvent
    {
        public delegate void LoseConditionCompletedHandler(LoseCondition condition);
        public static LoseConditionCompletedHandler OnLoseConditionCompleted;

        public static void Call(LoseCondition condition)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnLoseConditionCompleted?.Invoke(condition));
        }
    }
}
