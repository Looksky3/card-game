﻿namespace Assets.Scripts.Game.Battle.Event
{
    public class NewRoundBeginsEvent
    {
        public delegate void NewRoundBeginsEventHandler();
        public static NewRoundBeginsEventHandler OnRoundBegins;

        public static void Call()
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnRoundBegins?.Invoke());
        }
    }
}
