﻿namespace Assets.Scripts.Game.Battle.Event
{
    public class GameEndsEvent
    {
        public delegate void GameEndsEventHandler(bool playerWins);
        public static GameEndsEventHandler OnGameEnds;

        public static void Call(bool playerWins)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnGameEnds?.Invoke(playerWins));
        }
    }
}
