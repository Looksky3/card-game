﻿using Assets.Scripts.CardScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class CardIsPlayedEvent
    {
        public delegate void CardIsPlayedEventHandler(Card playedCard);
        public static CardIsPlayedEventHandler OnCardIsPlayed;

        public static void Call(Card playedCard)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnCardIsPlayed?.Invoke(playedCard));
        }
    }
}
