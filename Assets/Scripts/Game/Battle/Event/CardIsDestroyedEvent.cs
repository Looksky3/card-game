﻿using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class CardIsDestroyedEvent
    {
        public delegate void CardIsDestroyedEventHandler(Entity card, bool isPlayerCard);
        public static CardIsDestroyedEventHandler OnCardIsDestroyed;

        public static void Call(Entity card, bool isPlayerCard)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnCardIsDestroyed?.Invoke(card, isPlayerCard));
        }
    }
}
