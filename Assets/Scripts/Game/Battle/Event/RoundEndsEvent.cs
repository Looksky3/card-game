﻿namespace Assets.Scripts.Game.Battle.Event
{
    public class RoundEndsEvent
    {
        public delegate void RoundEndsEventHandler();
        public static RoundEndsEventHandler OnRoundEnds;

        public static void Call()
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnRoundEnds?.Invoke());
        }
    }
}
