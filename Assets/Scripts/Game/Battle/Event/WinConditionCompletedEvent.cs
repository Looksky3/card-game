﻿using Assets.Scripts.Game.LevelScripts.WinConditions;

namespace Assets.Scripts.Game.Battle.Event
{
    public class WinConditionCompletedEvent
    {
        public delegate void WinConditionCompletedHandler(WinCondition condition);
        public static WinConditionCompletedHandler OnWinConditionCompleted;

        public static void Call(WinCondition condition)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnWinConditionCompleted?.Invoke(condition));
        }
    }
}
