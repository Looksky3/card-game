﻿using Assets.Scripts.CardScripts.EntityScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class EnemySpawnedEvent
    {
        public delegate void EnemyCardEntersBattlefieldEventHandler(Entity enemy);
        public static EnemyCardEntersBattlefieldEventHandler OnEnemySpawned;

        public static void Call(Entity enemy)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnEnemySpawned?.Invoke(enemy));
        }
    }
}
