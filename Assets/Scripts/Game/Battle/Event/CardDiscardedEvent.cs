﻿using Assets.Scripts.CardScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class CardDiscardedEvent
    {
        public delegate void CardDiscardedEventHandler(Card[] cards);
        public static CardDiscardedEventHandler OnCardsDiscarded;

        public static void Call(params Card[] cards)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnCardsDiscarded?.Invoke(cards));
        }
    }
}
