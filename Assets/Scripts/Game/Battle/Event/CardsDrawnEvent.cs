﻿using Assets.Scripts.CardScripts;

namespace Assets.Scripts.Game.Battle.Event
{
    public class CardsDrawnEvent
    {
        public delegate void CardDrawnEventHandler(Card[] drawnCards);
        public static CardDrawnEventHandler OnCardsDrawn;

        public static void Call(params Card[] drawnCards)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnCardsDrawn?.Invoke(drawnCards));
        }
    }
}
