﻿using Assets.Scripts.CardScripts;
using Assets.Scripts.CardScripts.EntityScripts;
using Assets.Scripts.CardScripts.EntityScripts.SpecialAbility;
using Assets.Scripts.Game.Battle.Event;
using Assets.Scripts.Game.LevelScripts;
using Assets.Scripts.Game.LevelScripts.LoseConditions;
using Assets.Scripts.Game.LevelScripts.WinConditions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Game.Battle
{
    public class BattleManager
    {
        private Deck _deck;
        private Level _level;
        private Entity _commander;
        private List<Entity> _playerCards = new List<Entity>(), _enemyCards = new List<Entity>();

        public void Start(Deck deck, Entity commander, Level level)
        {
            Initialize(deck, commander, level);
        }

        private void Initialize(Deck deck, Entity commander, Level level)
        {
            _deck = deck;
            _level = level;
            _commander = commander;
            CardIsPlayedEvent.OnCardIsPlayed += OnCardIsPlayed;
            RoundEndsEvent.OnRoundEnds += OnRoundEnds;
            NewRoundBeginsEvent.OnRoundBegins += OnRoundBegins;
            PlayerCardEntersBattlefieldEvent.OnPlayerCardEntersBattlefield += OnPlayerCardEntersBattlefied;
            EnemySpawnedEvent.OnEnemySpawned += OnEnemySpawned;
            CardIsDestroyedEvent.OnCardIsDestroyed += OnCardIsDestroyed;
            GameEndsEvent.OnGameEnds += OnGameEnds;
            WinConditionCompletedEvent.OnWinConditionCompleted += OnWinConditionCompleted;
            LoseConditionCompletedEvent.OnLoseConditionCompleted += OnLoseConditionCompleted;
            SetStartingPositionPlayer(commander);
            SetStartingPositionEnemy(level);
            StartRound();
        }

        private void OnGameEnds(bool playerWins)
        {
            Uninitialize();
        }

        private void Uninitialize()
        {
            CardIsPlayedEvent.OnCardIsPlayed -= OnCardIsPlayed;
            RoundEndsEvent.OnRoundEnds -= OnRoundEnds;
            NewRoundBeginsEvent.OnRoundBegins -= OnRoundBegins;
            PlayerCardEntersBattlefieldEvent.OnPlayerCardEntersBattlefield -= OnPlayerCardEntersBattlefied;
            EnemySpawnedEvent.OnEnemySpawned -= OnEnemySpawned;
            CardIsDestroyedEvent.OnCardIsDestroyed -= OnCardIsDestroyed;
            GameEndsEvent.OnGameEnds -= OnGameEnds;
            
            _playerCards.Clear();
            _enemyCards.Clear();
            _deck = null;
            _level = null;
            _commander = null;
            ResourcesChangedEvent.Call(0);
        }

        private void OnRoundEnds()
        {
            Battle();
            
            NewRoundBeginsEvent.Call();
        }

        private void OnWinConditionCompleted(WinCondition condition)
        {
            if (_level.WinConditions.FirstOrDefault(l => l.Completed == false) == default(WinCondition))
                GameEndsEvent.Call(true);
        }

        private void OnLoseConditionCompleted(LoseCondition condition)
        {
            if (_level.LoseConditions.FirstOrDefault(l => l.Completed == false) == default(LoseCondition))
                GameEndsEvent.Call(false);
        }

        private void OnRoundBegins()
        {
            var _spawnAbilities = _playerCards.SelectMany(e => e.Abilities.Where(a => a is SpawnUnit)).ToArray();
            foreach (var playerSpawn in _spawnAbilities)
            {
                foreach (var card in (playerSpawn as SpawnUnit).Cards)
                {
                    PlayerCardEntersBattlefieldEvent.Call(Card.Read<Entity>(card));
                }
            }
            _spawnAbilities = _enemyCards.SelectMany(e => e.Abilities.Where(a => a is SpawnUnit)).ToArray();
            foreach (var enemySpawn in _spawnAbilities)
            {
                foreach (var card in (enemySpawn as SpawnUnit).Cards)
                {
                    EnemySpawnedEvent.Call(Card.Read<Entity>(card));
                }
            }
            StartRound();
        }

        private void OnPlayerCardEntersBattlefied(Entity card)
        {
            _playerCards.Add(card);
            var addedCards = card.Abilities.Where(a => a is AddCardToBattlefield).SelectMany(a => (a as AddCardToBattlefield)?.Cards);
            foreach (var addedCard in addedCards)
            {
                PlayerCardEntersBattlefieldEvent.Call(Card.Read<Entity>(addedCard));
            }
        }

        private void OnEnemySpawned(Entity card)
        {
            _enemyCards.Add(card);
            var addedCards = card.Abilities.Where(a => a is AddCardToBattlefield).SelectMany(a => (a as AddCardToBattlefield)?.Cards);
            foreach (var addedCard in addedCards)
            {
                EnemySpawnedEvent.Call(Card.Read<Entity>(addedCard));
            }
        }

        private void OnCardIsDestroyed(Entity card, bool isPlayerCharacter)
        {
            if (isPlayerCharacter)
            {
                _playerCards.Remove(card);
                foreach (var loseCondition in _level.LoseConditions)
                {
                    LoseUnit loseUnit = loseCondition as LoseUnit;
                    if (loseUnit != null)
                        loseUnit.IsCompleted(card);
                    else
                    {
                        LoseCommander loseCommander = loseCondition as LoseCommander;
                        if (loseCommander != null)
                            loseCommander.IsCompleted(_commander, card);
                    }
                }
            }
            else
            {
                _enemyCards.Remove(card);
                foreach (var winCondition in _level.WinConditions)
                {
                    KillEnemy killEnemy = winCondition as KillEnemy;
                    if (killEnemy != null)
                        killEnemy.IsCompleted(card);
                    else
                    {
                        KillAllEnemies killAllEnemies = winCondition as KillAllEnemies;
                        if (killAllEnemies != null)
                            killAllEnemies.IsCompleted(_enemyCards.ToArray());
                    }
                }
            }
        }

        private void OnCardIsPlayed(Card playedCard)
        {
            if (playedCard is Entity entiy)
                PlayerCardEntersBattlefieldEvent.Call(entiy);
        }

        private void SetStartingPositionPlayer(Entity commander)
        {
            PlayerCardEntersBattlefieldEvent.Call(commander);
        }

        private void SetStartingPositionEnemy(Level level)
        {
            foreach (var enemy in level.Enemies)
            {
                var enemyEntity = Card.Read<Entity>(enemy.Card);
                for (var i = 0; i < enemy.Amount; i++)
                {
                    EnemySpawnedEvent.Call(enemyEntity.Clone());
                }
            }
        }

        private void Battle()
        {
            var playerCardsByPriority = _playerCards.OrderByDescending(c => c.Priority).GroupBy(c => c.Priority);
            var enemyCardsByPriority = _enemyCards.OrderByDescending(c => c.Priority).GroupBy(c => c.Priority);
            ExecuteForEachCardPerPriority((Entity card) => Damage(card, enemyCardsByPriority, false), playerCardsByPriority);
            ExecuteForEachCardPerPriority((Entity card) => Damage(card, playerCardsByPriority, true), enemyCardsByPriority);
            var deadCards = _playerCards.Where(e => e.Health <= 0).ToArray();
            foreach (var entity in deadCards)
            {
                CardIsDestroyedEvent.Call(entity, true);
            }
            deadCards = _enemyCards.Where(e => e.Health <= 0).ToArray();
            foreach (var entity in deadCards)
            {
                CardIsDestroyedEvent.Call(entity, false);
            }
        }

        private void ExecuteForEachCardPerPriority(Action<Entity> action, IEnumerable<IGrouping<int, Entity>> cardsByPriority)
        {
            foreach (var cardsOfPriority in cardsByPriority)
            {
                foreach (var card in cardsOfPriority)
                {
                    action(card);
                }
            }
        }

        private void Damage(Entity attackingCard, IEnumerable<IGrouping<int, Entity>> defendingCardsByPriority, bool isPlayerAttacked)
        {
            foreach (var defendingCardsOfPriority in defendingCardsByPriority)
            {
                foreach (var card in defendingCardsOfPriority)
                {
                    if (card.Health > 0)
                    {
                        card.Health -= attackingCard.Damage;
                        CardIsAttackedEvent.Call(card, attackingCard, isPlayerAttacked);
                        Debug.Log(card.Name + " is damaged by: " + attackingCard.Name + " for " + attackingCard.Damage + ". Now it only has: " + card.Health + " hit points left.");
                        return;
                    }
                }
            }
        }

        private void StartRound()
        {
            _deck.Draw(2);
            ResourcesChangedEvent.Call(ResourcesChangedEvent.ResourceAmount + 2);
        }
    }
}
