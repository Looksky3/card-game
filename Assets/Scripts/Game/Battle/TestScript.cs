﻿using Assets.Scripts.CardScripts;
using Assets.Scripts.CardScripts.EntityScripts;
using Assets.Scripts.Game.LevelScripts;
using UnityEngine;

namespace Assets.Scripts.Game.Battle
{
    public class TestScript : MonoBehaviour
    {
        public void Start()
        {
            Deck deck = new Deck(Card.Read<Entity>("Pidgeon"), Card.Read<Entity>("Pidgeon"));
            BattleManager battleManager = new BattleManager();
            battleManager.Start(deck, Card.Read<Entity>("Sena"), Level.Read("Level1"));
        }
    }
}
