﻿using Assets.Scripts.CardScripts;
using Assets.Scripts.Game.Battle.Event;

namespace Assets.Scripts.Game.Battle.CardHandler
{
    public static class CardPlayer
    {
        public static TryPlayResult TryPlay(Card card)
        {
            if (card.Cost > ResourcesChangedEvent.ResourceAmount)
                return TryPlayResult.NotEnoughResources;
            else
            {
                CardIsPlayedEvent.Call(card);
                ResourcesChangedEvent.Call(ResourcesChangedEvent.ResourceAmount - card.Cost);
                return TryPlayResult.Succes;
            }
        }
    }
}
