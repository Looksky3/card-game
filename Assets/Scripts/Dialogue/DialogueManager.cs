﻿using Assets.Scripts.Dialogue.Dialogues;
using Assets.Scripts.Dialogue.Events;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Dialogue
{
    public class DialogueManager
    {
        public void Play(DialogueTree dialogueTree)
        {
            Task.Run(() => PlayDialogue(dialogueTree));
        }

        private void PlayDialogue(DialogueTree dialogueTree)
        {
            DialogueLine currentLine = dialogueTree.Dialogues.FirstOrDefault();
            if (currentLine == default(DialogueLine))
                return;

            DialogueStartsEvent.Call(dialogueTree.Environment);

            while (currentLine != default(DialogueLine))
            {
                string followupId = currentLine is SingleLine ? WaitForFollowupId(currentLine as SingleLine) :
                                    currentLine is Question ? WaitForFollowupId(currentLine as Question) :
                                    "";
                currentLine = GetNextLine(dialogueTree, followupId);
            }

            DialogueEndsEvent.Call();
        }

        private string WaitForFollowupId(SingleLine singleLine)
        {
            bool ended = false;
            string followupId = "";

            Task.Run(() =>
            {
                Action listener = null;
                listener = () =>
                {
                    followupId = singleLine.FollowupId;
                    ended = true;
                    SingleLineEndsEvent.OnSingleLineEnds -= listener.Invoke;
                };
                SingleLineEndsEvent.OnSingleLineEnds += listener.Invoke;

                SingleLineStartsEvent.Call(singleLine);
            });

            while (ended == false)
            {
                Thread.Sleep(100);
            }

            return followupId;
        }

        private string WaitForFollowupId(Question question)
        {
            bool ended = false;
            string followupId = "";

            Task.Run(() =>
                {
                    Action<Answer> listener = null;
                    listener = (Answer answer) =>
                    {
                        followupId = answer.FollowupId;
                        ended = true;
                        Debug.Log(question.Id);
                        QuestionEndsEvent.OnQuestionEnds -= listener.Invoke;
                    };
                    QuestionEndsEvent.OnQuestionEnds += listener.Invoke;

                    QuestionStartsEvent.Call(question);
                });

            while (ended == false)
            {
                Thread.Sleep(100);
            }

            return followupId;
        }

        private DialogueLine GetNextLine(DialogueTree dialogueTree, string followupId)
        {
            return dialogueTree.Dialogues.FirstOrDefault(d => d.Id == followupId);
        }
    }
}
