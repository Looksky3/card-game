﻿using Assets.Scripts.Dialogue.Dialogues;
using System.IO;
using System.Xml.Serialization;

namespace Assets.Scripts.Dialogue
{
    public class DialogueTree
    {
        [XmlArrayItem(Type = typeof(SingleLine))]
        [XmlArrayItem(Type = typeof(Question))]
        [XmlArray("Dialogues"), XmlArrayItem("Dialogue")] public DialogueLine[] Dialogues;

        [XmlElement("Environment")] public string Environment;

        public static DialogueTree Read(string name)
        {
            var serializer = new XmlSerializer(typeof(DialogueTree));
            using (var stream = new FileStream(@"Assets/Resources/Dialogues/" + name + ".xml", FileMode.Open))
            {
                var readCard = serializer.Deserialize(stream) as DialogueTree;
                return readCard;
            }
        }
    }
}
