﻿using UnityEngine;

namespace Assets.Scripts.Dialogue
{
    public class TestScript : MonoBehaviour
    {
        public void Start()
        {
            new DialogueManager().Play(DialogueTree.Read("new"));
        }
    }
}
