﻿using System.Xml.Serialization;

namespace Assets.Scripts.Dialogue.Dialogues
{
    public class DialogueLine
    {
        [XmlElement("Id")] public string Id;
        [XmlElement("Text")] public string Text;
        [XmlElement("Speaker")] public string Speaker;
    }
}
