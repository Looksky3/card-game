﻿using System.Xml.Serialization;

namespace Assets.Scripts.Dialogue.Dialogues
{
    public class SingleLine : DialogueLine
    {
        [XmlElement("FollowupId")] public string FollowupId;
    }
}
