﻿using System.Xml.Serialization;

namespace Assets.Scripts.Dialogue.Dialogues
{
    public class Question : DialogueLine
    {
        [XmlArray("Answers"), XmlArrayItem("Answer")] public Answer[] Answers;
    }
}
