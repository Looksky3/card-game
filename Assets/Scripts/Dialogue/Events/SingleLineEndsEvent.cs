﻿namespace Assets.Scripts.Dialogue.Events
{
    public class SingleLineEndsEvent
    {
        public delegate void SingleLineEndsEventHandler();
        public static SingleLineEndsEventHandler OnSingleLineEnds;

        public static void Call()
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnSingleLineEnds?.Invoke());
        }
    }
}
