﻿using Assets.Scripts.Dialogue.Dialogues;

namespace Assets.Scripts.Dialogue.Events
{
    public class QuestionStartsEvent
    {
        public delegate void QuestionStartsEventHandler(Question line);
        public static QuestionStartsEventHandler OnQuestionStarts;

        public static void Call(Question line)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnQuestionStarts?.Invoke(line));
        }
    }
}
