﻿using Assets.Scripts.Dialogue.Dialogues;

namespace Assets.Scripts.Dialogue.Events
{
    public class QuestionEndsEvent
    {
        public delegate void QuestionEndsEventHandler(Answer answer);
        public static QuestionEndsEventHandler OnQuestionEnds;

        public static void Call(Answer line)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnQuestionEnds?.Invoke(line));
        }
    }
}
