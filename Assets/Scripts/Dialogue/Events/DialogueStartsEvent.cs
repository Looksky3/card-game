﻿namespace Assets.Scripts.Dialogue.Events
{
    public class DialogueStartsEvent
    {
        public delegate void DialogueStartsEventHandler(string environment);
        public static DialogueStartsEventHandler OnDialogueStarts;

        public static void Call(string environment)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnDialogueStarts?.Invoke(environment));
        }
    }
}