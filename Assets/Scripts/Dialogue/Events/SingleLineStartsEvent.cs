﻿using Assets.Scripts.Dialogue.Dialogues;

namespace Assets.Scripts.Dialogue.Events
{
    public class SingleLineStartsEvent
    {
        public delegate void SingleLineStartsEventHandler(SingleLine line);
        public static SingleLineStartsEventHandler OnSingleLineStarts;

        public static void Call(SingleLine line)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnSingleLineStarts?.Invoke(line));
        }
    }
}
