﻿namespace Assets.Scripts.Dialogue.Events
{
    public class DialogueEndsEvent
    {
        public delegate void DialogueEndsEventHandler();
        public static DialogueEndsEventHandler OnDialogueEnds;

        public static void Call()
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnDialogueEnds?.Invoke());
        }
    }
}