﻿namespace Assets.Scripts.ObjectPooling
{
    public interface IPoolableFactory<out T> where T : IPoolable<T>
    {
        T Create();
    }
}
