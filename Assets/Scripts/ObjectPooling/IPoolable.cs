﻿using System;

namespace Assets.Scripts.ObjectPooling
{
    public interface IPoolable<T> where T : IPoolable<T>
    {
        void SetInActive();
        void SetActive();
    }
}
