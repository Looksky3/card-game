﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.ObjectPooling
{
    public class PoolManager<T> where T : IPoolable<T>
    {
        private readonly List<T> _inactiveObjects = new List<T>();
        public readonly List<T> ActiveObjects = new List<T>();
        private readonly IPoolableFactory<T> _factory;
        public int Count => ActiveObjects.Count;

        public PoolManager(IPoolableFactory<T> factory)
        {
            _factory = factory;
        }

        public T GetObject()
        {
            if (_inactiveObjects.Count > 0)
            {
                var theObject = _inactiveObjects[0];
                _inactiveObjects.Remove(theObject);
                theObject.SetActive();
                ActiveObjects.Add(theObject);
                return theObject;
            }
            else
            {
                var theObject = _factory.Create();
                ActiveObjects.Add(theObject);
                return theObject;
            }
        }

        public void RemoveAllObjects()
        {
            DoForAllObjects(RemoveObject);
            ActiveObjects.Clear();
        }

        public void RemoveObject(T theObject)
        {
            theObject.SetInActive();
            ActiveObjects.Remove(theObject);
            _inactiveObjects.Add(theObject);
        }

        public void DoForAllObjects(Action<T> action)
        {
            for (var i = ActiveObjects.Count - 1; i >= 0; i--)
            {
                action(ActiveObjects[i]);
            }
        }
    }
}
